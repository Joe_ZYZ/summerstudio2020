	#!/bin/bash
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
xhost + $ip

# docker run --rm -it \
# 	-e DISPLAY=$ip:0 \
# 	-p 8888:8888 \
# 	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
#         --privileged \
# 	--volume="/Users/liuye/UTS/summer_studio/week1.2/summerstudio2020" \
# 	benthepleb/summerstudio2020:latest \
# 	bash

docker run --rm -it \
	-e DISPLAY=$ip:0 \
	-p 8888:8888 \
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--volume="$HOME/studio:/root/studio" \
	--privileged \
	--volume="/Users/liuqingyang/UTS/summer/summerstudio2020:/root/studio1" \
	benthepleb/summerstudio2020:latest \
	bash
